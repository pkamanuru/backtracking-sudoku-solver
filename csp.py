import random
###########################################
# you need to implement five funcitons here
###########################################
from sudoku_class import sudoku 
def backtracking(filename):
    """
    returns solution or -1 (in case of failure)
    """
    sudoku_game = sudoku(filename) 
    
    # Debug #
    # sudoku_game.testing_area()

    # @todo: Check and format accordingly    
    # Required format ofoutput
    # return ([[],[]], 0)

    result = rec_backtracking(sudoku_game)

    if result == -1:    # Failure
        return ([],-1)
    else:               # Success
        return (result.sudoku_board, result.consistency_checks_count)


def rec_backtracking(sudoku_game):
    """
    returns -1 in case of failure
    Success: return (<solution: list of list>, # of consistency checks)
    """
    sudoku_game.consistency_checks_count += 1
    # if assignment is complete return the solution else failure
    if sudoku_game.is_assignment_complete():
        return sudoku_game

    # variables declared: used in checking consistency of the new_sudoku_game_state
    assigned_row = 0
    assigned_col = 0

    # NEW_CODE : Without making copy of game object
    un_row, un_col = sudoku_game.get_un_assigned_cell()
    if un_row == -1 or un_col == -1:
        if sudoku_game.is_assignment_complete():
            return sudoku_game
        else:
            return -1

    for dom_val in range(1, sudoku_game.N+1):

        sudoku_game.modify_sudoku_game(un_row, un_col, dom_val)
        if sudoku_game.is_consistent(un_row, un_col):
            result = rec_backtracking(sudoku_game)

            if result != -1:
                return result

        sudoku_game.modify_sudoku_game(un_row, un_col, -1)
    return -1

    # # OLD_CODE : creates copy of game object
    # #iterate in all possible moves and generate new game state
    # for new_sudoku_game, assigned_row, assigned_col in sudoku_game.get_all_next_states():
        
    #     # Debug #
    #     # print "new_game_state:\n", new_sudoku_game.sudoku_board, "\n"
        
    #     # check if is consistent
    #     if new_sudoku_game.is_consistent(assigned_row, assigned_col):

    #         result = rec_backtracking(new_sudoku_game)

    #         if result != -1:
    #             return result
    #         else:
    #             continue
    # return -1

def backtrackingMRV(filename):
    """
    returns solution or -1 (in case of failure)
    """
    sudoku_game = sudoku(filename) 
    
    result = rec_backtracking_mrv(sudoku_game)

    if result == -1:    # Failure
        return ([],-1)
    else:               # Success
        return (result.sudoku_board, result.consistency_checks_count)

def rec_backtracking_mrv(sudoku_game):
    """
    returns -1 in case of failure
    Success: return (<solution: list of list>, # of consistency checks)
    """
    sudoku_game.consistency_checks_count += 1
    # if assignment is complete return the solution else failure
    if sudoku_game.is_assignment_complete():
        return sudoku_game

    # variables declared: used in checking consistency of the new_sudoku_game_state
    assigned_row = 0
    assigned_col = 0

    # NEW_CODE : Without making copy of game object
    un_row, un_col = sudoku_game.get_un_assigned_mrv_cell()
    if un_row == -1 or un_col == -1:
        if sudoku_game.is_assignment_complete():
            return sudoku_game
        else:
            return -1

    for dom_val in sudoku_game.get_dom_val_in_lcv_order(un_row, un_col):

        sudoku_game.modify_sudoku_game(un_row, un_col, dom_val)
        if sudoku_game.is_consistent(un_row, un_col):
            result = rec_backtracking_mrv(sudoku_game)

            if result != -1:
                return result

        sudoku_game.modify_sudoku_game(un_row, un_col, -1)
    return -1


def backtrackingMRVfwd(filename):
    """
    returns solution or -1 (in case of failure)
    """
    sudoku_game = sudoku(filename) 
    
    result = rec_backtracking_mrv_fwd_chk(sudoku_game)

    if result == -1:    # Failure
        return ([],-1)
    else:               # Success
        return (result.sudoku_board, result.consistency_checks_count)

def rec_backtracking_mrv_fwd_chk(sudoku_game):
    """
    returns -1 in case of failure
    Success: return (<solution: list of list>, # of consistency checks)
    """
    sudoku_game.consistency_checks_count += 1
    # if assignment is complete return the solution else failure
    if sudoku_game.is_assignment_complete():
        return sudoku_game

    # variables declared: used in checking consistency of the new_sudoku_game_state
    assigned_row = 0
    assigned_col = 0

    # NEW_CODE : Without making copy of game object
    un_row, un_col = sudoku_game.get_un_assigned_mrv_cell()
    if un_row == -1 or un_col == -1:
        if sudoku_game.is_assignment_complete():
            return sudoku_game
        else:
            return -1

    for dom_val in sudoku_game.get_dom_val_in_lcv_order(un_row, un_col):

        sudoku_game.modify_sudoku_game(un_row, un_col, dom_val)

        if sudoku_game.is_consistent(un_row, un_col) and sudoku_game.all_cells_alive():
            result = rec_backtracking_mrv_fwd_chk(sudoku_game)

            if result != -1:
                return result

        sudoku_game.modify_sudoku_game(un_row, un_col, -1)
    return -1

def backtrackingMRVcp(filename):
    """
    returns solution or -1 (in case of failure)
    """
    sudoku_game = sudoku(filename) 
    
    result = rec_backtracking_mrv_cp(sudoku_game)

    if result == -1:    # Failure
        return ([],-1)
    else:               # Success
        return (result.sudoku_board, result.consistency_checks_count)

def rec_backtracking_mrv_cp(sudoku_game):
    """
    returns -1 in case of failure
    Success: return (<solution: list of list>, # of consistency checks)
    """
    sudoku_game.consistency_checks_count += 1
    # if assignment is complete return the solution else failure
    if sudoku_game.is_assignment_complete():
        return sudoku_game

    # variables declared: used in checking consistency of the new_sudoku_game_state
    assigned_row = 0
    assigned_col = 0

    # NEW_CODE : Without making copy of game object
    un_row, un_col = sudoku_game.get_un_assigned_mrv_cell()
    if un_row == -1 or un_col == -1:
        if sudoku_game.is_assignment_complete():
            return sudoku_game
        else:
            return -1

    for dom_val in sudoku_game.get_dom_val_in_lcv_order(un_row, un_col):

        sudoku_game.modify_sudoku_game_and_propagate_constraint(un_row, un_col, dom_val)

        if sudoku_game.is_consistent(un_row, un_col) and sudoku_game.all_cells_alive():
            result = rec_backtracking_mrv_cp(sudoku_game)

            if result != -1:
                return result

        sudoku_game.modify_sudoku_game_and_propagate_constraint(un_row, un_col, -1)
    return -1

def minConflict(filename):

    MAX_STEPS = 100000 
    sudoku_game = sudoku(filename) 
    result = min_conflict_helper(sudoku_game, MAX_STEPS)

    if result == -1:    # Failure
        return ([],result.consistency_checks_count)
    else:               # Success
        return (result.sudoku_board, result.consistency_checks_count)

def min_conflict_helper(sudoku_game,max_steps=10):
    """
    Returns a solved instance of sudoku_game 
    else, (Failure): returns sudoku_game([], consistency_checks_count)
    """

    #Making the Assignment Complete
    sudoku_game.assign_blank_spaces()

    for itr_no in range(0, max_steps):
        
        sudoku_game.consistency_checks_count += 1       

        (row, col) = sudoku_game.get_conflicted_cell()  # randomly chosen conflicted cell
                                                        # return (-1, -1) when no conflic present
        
        # Debug #
        # print "chosen variable= ", row, col, "\n"

        if row == -1 and col == -1:                     # game solved
            return sudoku_game

        val = sudoku_game.get_min_conflict_value(row, col)

        # Debug #
        # print "chosen value= ", val, "\n"

        sudoku_game.sudoku_board[row][col]=val          # Assumption: conflicts will be re-calculated / updated
           
    sudoku_game.sudoku_board = []                       # @Todo: Scope of optimization. update only changed section.
    return sudoku_game






    





