import copy
import random

class sudoku():

    def __init__(self,filename=None):
        if filename is not None:
            self.N , self.M , self.K , self.sudoku_board =  self.parse_sudoku_input(filename)
            self.N = int ( self.N )
            self.M = int ( self.M )
            self.K = int ( self.K )
            self.no_of_blocks_assigned = 0
            self.consistency_checks_count = 0
            self.dead_cells = set()
            self.possible_values_map = [[dict((val,0) for val in range(1,self.N+1)) for j in range(self.N)] for k in range(self.N)]
            self.possible_values = [[set([i for i in range(1,self.N+1)]) for j in range(self.N)] for k in range(self.N)]            
            self.assigned = [[False for x in range(0,self.N)] for y in range(0,self.N)]

            for row in self.sudoku_board:
                for element in row:
                    if element != -1:
                        self.no_of_blocks_assigned += 1

            for row in range(0, self.N):
                for col in range(0, self.N):
                    if self.sudoku_board[row][col] != -1:
                        self.assigned[row][col] = True 
            # Debug #
            # print "superman ", self.possible_values_map, "\n"
        else:
            self.N=0
            self.M=0
            self.K=0
            self.sudoku_board=[]
            self.no_of_blocks_assigned=0

    def parse_sudoku_input(self,file_name):
        game_file=open(file_name,'r')
        first_line=game_file.readline()
        N,M,K=first_line.replace('\n','').replace(';','').split(',')
        
        # Debug #
        # print "N M K ", N, M, K, "\n"

        #Reading Each Cell input
        game=[]
        for line in game_file:
            game.append(line.replace('\n','').replace(';','').split(','))

        # translating every string val to int val
        N = int(N)
        M = int(M)
        K = int(K)

        for row in range(0,N):
            for col in range(0,N):
                if game[row][col] == '-':
                    game[row][col] = -1
                else:
                    game[row][col] = int(game[row][col]) 

        return N,M,K,game

    def get_copy(self):
        return copy.deepcopy(self)

    def modify_sudoku_game(self, row, col, value):

        if self.sudoku_board[row][col] == -1:
            self.assigned[row][col] = True
            self.no_of_blocks_assigned += 1
            
            # for all neighbours, reduce the value of this value from possible values
            # For row neighbours
            for col_i in range(0,self.N):
                if col_i != col:                
                    self.possible_values_map[row][col_i][value] -= 1
                    if value in self.possible_values[row][col_i]:
                        self.possible_values[row][col_i].remove(value)
                    if len(self.possible_values[row][col_i]) == 0 and self.assigned[row][col_i] == False:
                        self.dead_cells.add((row,col_i))
                        # Debug #
                        # print row, col_i, "dead\n"

            # For col neighbours
            for row_i in range(0,self.N):
                if row_i != row:                
                    self.possible_values_map[row_i][col][value] -= 1
                    if value in self.possible_values[row_i][col]:
                        self.possible_values[row_i][col].remove(value)
                    if len(self.possible_values[row_i][col]) == 0 and self.assigned[row_i][col] == False:
                        self.dead_cells.add((row_i,col))
                        # Debug #
                        # print row_i, col, "dead\n"

            # For grid neighbours
            grid_i_pos = row - (row)%self.M
            grid_j_pos = col - (col)%self.K  

            for row_i in range(grid_i_pos, grid_i_pos+self.M):
                for col_i in range(grid_j_pos, grid_j_pos+self.K):
                    if row_i != row and col_i != col:                    
                        self.possible_values_map[row_i][col_i][value] -= 1
                        if value in self.possible_values[row_i][col_i]:
                            self.possible_values[row_i][col_i].remove(value)
                        if len(self.possible_values[row_i][col_i]) == 0 and self.assigned[row_i][col_i] == False:
                            self.dead_cells.add((row_i,col_i))
                            # Debug #
                            # print row_i, col_i, "dead\n"

        elif value == -1:
            self.assigned[row][col] = False            
            self.no_of_blocks_assigned -= 1
            # for all neighbours, increase the value of this value from possible values
            # For row neighbours
            for col_i in range(0,self.N):
                if col_i != col:                    
                    self.possible_values_map[row][col_i][self.sudoku_board[row][col]] += 1
                    if self.possible_values_map[row][col_i][self.sudoku_board[row][col]] == 0:
                        self.possible_values[row][col_i].add(self.sudoku_board[row][col]) 
                        if (row, col_i) in self.dead_cells:
                            self.dead_cells.remove((row, col_i))
                            # Debug #
                            # print row, col_i, "alive\n"                        

            # For col neighbours
            for row_i in range(0,self.N):
                if row_i != row:                    
                    self.possible_values_map[row_i][col][self.sudoku_board[row][col]] += 1
                    if self.possible_values_map[row_i][col][self.sudoku_board[row][col]] == 0:
                        self.possible_values[row_i][col].add(self.sudoku_board[row][col])
                        if (row_i, col) in self.dead_cells:
                            self.dead_cells.remove((row_i, col))
                            # Debug # 
                            # print row_i, col, "alive\n"

            # For grid neighbours
            grid_i_pos = row - (row)%self.M
            grid_j_pos = col - (col)%self.K         
            for row_i in range(grid_i_pos, grid_i_pos+self.M):
                for col_i in range(grid_j_pos, grid_j_pos+self.K):
                    if row_i != row and col_i != col:                        
                        self.possible_values_map[row_i][col_i][self.sudoku_board[row][col]] += 1
                        if self.possible_values_map[row_i][col_i][self.sudoku_board[row][col]] == 0:
                            self.possible_values[row_i][col_i].add(self.sudoku_board[row][col])        
                            if (row_i, col_i) in self.dead_cells:
                                self.dead_cells.remove((row_i,col_i))
                                # Debug #
                                # print row_i, col_i, "alive\n"

        self.sudoku_board[row][col] = value

    def modify_sudoku_game_and_propagate_constraint(self, row, col, value):
        """
        Does the same job as modifying sudoku game but also checks if due last change,
        if any variable have just one value left,
        if so propagate constraint and delete that value from the neighbours
        """
        if self.sudoku_board[row][col] == -1:
            self.assigned[row][col] = True
            self.no_of_blocks_assigned += 1
            
            # for all neighbours, reduce the value of this value from possible values
            # For row neighbours
            for col_i in range(0,self.N):
                if col_i != col:

                    self.possible_values_map[row][col_i][value] -= 1
                    if value in self.possible_values[row][col_i] and self.assigned[row][col_i] == False:
                        self.possible_values[row][col_i].remove(value)
                    
                    # check and propagate
                    if len(self.possible_values[row][col_i]) == 1 and self.assigned[row][col_i] == False:
                        (value_to_be_deleted,) =  self.possible_values[row][col_i]
                        self.delete_val_from_neighbour(row, col_i, value_to_be_deleted, row, col)

                    if len(self.possible_values[row][col_i]) == 0 and self.assigned[row][col_i] == False:
                        self.dead_cells.add((row,col_i))
                        # Debug #
                        # print "at:", row, col, " row: marking_dead", row, col_i, "\n", "current_status: ", self.sudoku_board, "\n assigned: ", self.assigned, "\n"

            # For col neighbours
            for row_i in range(0,self.N):
                if row_i != row:

                    self.possible_values_map[row_i][col][value] -= 1
                    if value in self.possible_values[row_i][col] and self.assigned[row_i][col] == False:
                        self.possible_values[row_i][col].remove(value)
                    
                    # check and propagate
                    if len(self.possible_values[row_i][col]) == 1 and self.assigned[row_i][col] == False:
                        (value_to_be_deleted,) =  self.possible_values[row_i][col]
                        self.delete_val_from_neighbour(row_i, col, value_to_be_deleted, row, col)
                        

                    if len(self.possible_values[row_i][col]) == 0 and self.assigned[row_i][col] == False:
                        self.dead_cells.add((row_i,col))
                        # Debug #
                        # print "col: marking_dead", row_i, col, "\n", "current_status: ", self.sudoku_board, "\n assigned: ", self.assigned, "\n"

            # For grid neighbours
            grid_i_pos = row - (row)%self.M
            grid_j_pos = col - (col)%self.K         
            for row_i in range(grid_i_pos, grid_i_pos+self.M):
                for col_i in range(grid_j_pos, grid_j_pos+self.K):
                    if row_i != row and col_i != col:                        
                        self.possible_values_map[row_i][col_i][value] -= 1
                        if value in self.possible_values[row_i][col_i] and self.assigned[row_i][col_i] == False:
                            self.possible_values[row_i][col_i].remove(value)
                    
                        # check and propagate
                        if len(self.possible_values[row_i][col_i]) == 1 and self.assigned[row_i][col_i] == False:
                            (value_to_be_deleted,) =  self.possible_values[row_i][col_i]
                            self.delete_val_from_neighbour(row_i, col_i, value_to_be_deleted, row, col)

                        if len(self.possible_values[row_i][col_i]) == 0 and self.assigned[row_i][col_i] == False:
                            self.dead_cells.add((row_i,col_i))
                            # Debug #
                            # print "at:", row, col, value, "grid: marking_dead", row_i, col_i, "\n", "current_status: ", self.sudoku_board, "\n assigned: ", self.assigned, "\n"                    


        elif value == -1:
            self.assigned[row][col] = False
            self.no_of_blocks_assigned -= 1
    
            # for all neighbours, increase the value of this value from possible values
            # For row neighbours
            for col_i in range(0,self.N):
                if col_i != col:                  
                    self.possible_values_map[row][col_i][self.sudoku_board[row][col]] += 1

                    if self.possible_values_map[row][col_i][self.sudoku_board[row][col]] == 0:
                        
                        if len(self.possible_values[row][col_i]) == 1:
                            (value_to_be_added,) = self.possible_values[row][col_i]
                            self.add_val_in_neighbour(row, col_i, value_to_be_added, row, col)

                        self.possible_values[row][col_i].add(self.sudoku_board[row][col])                    
                        if (row, col_i) in self.dead_cells:
                            self.dead_cells.remove((row, col_i))



            # For col neighbours
            for row_i in range(0,self.N):
                if row_i != row:                 
                    self.possible_values_map[row_i][col][self.sudoku_board[row][col]] += 1

                    if self.possible_values_map[row_i][col][self.sudoku_board[row][col]] == 0:
                        
                        if len(self.possible_values[row_i][col]) == 1:
                            (value_to_be_added,) = self.possible_values[row_i][col]
                            self.add_val_in_neighbour(row_i, col, value_to_be_added, row, col)

                        self.possible_values[row_i][col].add(self.sudoku_board[row][col])
                        if (row_i, col) in self.dead_cells:
                            self.dead_cells.remove((row_i, col))


            # For grid neighbours
            grid_i_pos = row - (row)%self.M
            grid_j_pos = col - (col)%self.K         
            for row_i in range(grid_i_pos, grid_i_pos+self.M):
                for col_i in range(grid_j_pos, grid_j_pos+self.K):
                    if row_i != row and col_i != col:                     
                        self.possible_values_map[row_i][col_i][self.sudoku_board[row][col]] += 1

                        if self.possible_values_map[row_i][col_i][self.sudoku_board[row][col]] == 0:

                            if len(self.possible_values[row_i][col_i]) == 1:
                                (value_to_be_added,) = self.possible_values[row_i][col_i]
                                self.add_val_in_neighbour(row_i, col_i, value_to_be_added, row, col)

                            self.possible_values[row_i][col_i].add(self.sudoku_board[row][col])        
                            if (row_i, col_i) in self.dead_cells:
                                self.dead_cells.remove((row_i,col_i))


        self.sudoku_board[row][col] = value


    def delete_val_from_neighbour(self, row, col, val, exclude_row, exclude_col):
        # row
        for col_i in range(0, self.N):
            if val in self.possible_values[row][col_i] and col_i != col and col_i != exclude_col:
                self.possible_values[row][col_i].remove(val)

        # col
        for row_i in range(0, self.N):
            if val in self.possible_values[row_i][col] and row_i != row and row_i != exclude_row:
                self.possible_values[row_i][col].remove(val)

        # grid values
        grid_i_pos = row - (row)%self.M
        grid_j_pos = col - (col)%self.K         
        for row_i in range(grid_i_pos, grid_i_pos+self.M):
            for col_i in range(grid_j_pos, grid_j_pos+self.K):
                if val in self.possible_values[row_i][col_i] and col_i != col and row_i != row and row_i != exclude_row and col_i != exclude_col:
                    self.possible_values[row_i][col_i].remove(val)

    def add_val_in_neighbour(self, row, col, val, exclude_row, exclude_col):
        for col_i in range(0, self.N):
            if col_i != col and col_i != exclude_col:
                self.possible_values[row][col_i].add(val)

        # col
        for row_i in range(0, self.N):
            if row_i != row and row_i != exclude_row:
                self.possible_values[row_i][col].add(val)

        # grid values
        grid_i_pos = row - (row)%self.M
        grid_j_pos = col - (col)%self.K         
        for row_i in range(grid_i_pos, grid_i_pos+self.M):
            for col_i in range(grid_j_pos, grid_j_pos+self.K):
                if row_i != row and col_i != col and row_i != exclude_row and col_i != exclude_col:
                    self.possible_values[row_i][col_i].add(val)



    def is_grid_complete(self, pos_i, pos_j):
        is_present=[False for x in range(self.N)]

        for row in range(pos_i, pos_i+self.M):
            for col in range(pos_j, pos_j+self.K):
                if self.sudoku_board[row][col] != -1:
                    is_present[self.sudoku_board[row][col]-1] = True
        

        if False in is_present:
            return False
        else:
            return True

    def is_column_complete(self, col):
        is_present=[False for x in range(self.N)]
        
        for row in range(0, self.N):
            if self.sudoku_board[row][col] != -1:
                is_present[self.sudoku_board[row][col]-1] = True
        
        if False in is_present:
            return False
        else:
            return True

    def is_row_complete(self, row):
        is_present=[False for x in range(self.N)]

        for col in range(0, self.N):
            if self.sudoku_board[row][col] != -1:
                is_present[self.sudoku_board[row][col]-1] = True

        if False in is_present:
            return False
        else:
            return True
 
    def is_assignment_complete(self):

        # Debug #
        # print "Number of assignments_done", self.no_of_blocks_assigned, "\n"

        # col and row check (Global row and col)
        if self.no_of_blocks_assigned == self.N*self.N:
            for i in range(0,self.N):
                if not self.is_row_complete(i) or not self.is_column_complete(i):
                    
                    # Debug #
                    # print self.sudoku_board[6]
                    # print "row_", i, self.is_row_complete(i), "\notcol_", i, self.is_column_complete(i), "\n"
                    # print "Some row, col failing: ", i, "\n"

                    return False
            i=0
            j=0
            while i<self.N:
                while j<self.N:
                    if not self.is_grid_complete(i,j):
                        
                        # Debug #
                        # print "grid failing i, j, val: ", i, j, "\n"

                        return False
                    j+=self.K
                i+=self.M
            return True
        return False

    def get_all_next_states(self):
        for row in range(0,self.N):
            for col in range(0,self.N):
                if  self.sudoku_board[row][col] == -1:
                    new_sudoku_board = self.get_copy()
                    for val in range(1,self.N+1):
                        new_sudoku_board.modify_sudoku_game(row, col, val)
                        yield new_sudoku_board, row, col

    def print_game_state(self):
        print self.sudoku, "\n"

    def print_game(self):
        print "N M K ",self.N , self.M , self.K, "\n"
        self.print_game_state()

    def is_row_consistent(self, row):
        is_present=[False for x in range(self.N)]

        for col in range(0,self.N):
            if self.sudoku_board[row][col] != -1:            
                if is_present[self.sudoku_board[row][col]-1] == True:
                    return False
                else:
                    is_present[self.sudoku_board[row][col]-1]=True
        
        return True

    def is_col_consistent(self, col):
        is_present=[False for x in range(self.N)]

        for row in range(0,self.N):
            if self.sudoku_board[row][col] != -1:
                if is_present[self.sudoku_board[row][col]-1] == True:
                    return False
                else:
                    is_present[self.sudoku_board[row][col]-1]=True
        
        return True

    def is_grid_consistent(self, pos_i, pos_j):
        is_present=[False for x in range(self.N)]

        for row in range(pos_i, pos_i+self.M):
            for col in range(pos_j, pos_j+self.K):
                
                # Debug #
                # print "row, col: ", row, col, "\n"
                # print "pos_i, pos_j, self.M, self.K: ", pos_i, pos_j, self.M, self.K, "\n"
                
                if self.sudoku_board[row][col] != -1:                            
                    if is_present[self.sudoku_board[row][col]-1] == True:
                        return False
                    else:
                        is_present[self.sudoku_board[row][col]-1]=True
        return True

    def is_consistent(self, row, col):

        # row check
        if not self.is_row_consistent(row):
            # print "row_check_failing\n"
            return False


        # col check 
        if not self.is_col_consistent(col):
            # print "col_check_failing\n"
            return False


        # grid check
        grid_i_pos = row - (row)%self.M
        grid_j_pos = col - (col)%self.K 
               
        if not self.is_grid_consistent(grid_i_pos,grid_j_pos):
            # print "grid_check_failing\n"
            return False        

        return True

    def get_un_assigned_cell(self):
        
        for row in range(0,self.N):
            for col in range(0,self.N):
                # print row, col, self.sudoku_board[row][col] 
                if self.sudoku_board[row][col] == -1:
                    # print "chala"
                    return (row, col)

        return (-1,-1)

    def get_un_assigned_mrv_cell(self):
        
        # Get a list of unassigned cells, check for optimizations later
        all_unassigned_cells = []
        for row in range(0,self.N):
            for col in range(0,self.N):
                if self.sudoku_board[row][col] == -1:
                    all_unassigned_cells.append((row,col))

        if len(all_unassigned_cells) == 0:
            return (-1,-1)

        # Create a list of tuples, (pos, remaining_values)
        sorted_unassigned_cells = []
        for cell in all_unassigned_cells:
            sorted_unassigned_cells.append( (cell, len(self.possible_values[cell[0]][cell[1]]) ) )

        sorted_unassigned_cells = sorted(sorted_unassigned_cells, key=lambda x: x[1])

        return sorted_unassigned_cells[0][0]

    def get_count_of_constraints(self, row, col, val):
        # how many neighbours have val in their possible values set
        # check value is in the pssible moves
        # val in possible_values[row][col] 

        constraint_count = 0

        # check for contraint count in row neighbours
        for col_i in range(0,self.N):
            if col_i != col:
                constraint_count += (val in self.possible_values[row][col_i])

        # check for contraint count in col neighbours
        for row_i in range(0,self.N):
            if row_i != row:
                constraint_count += (val in self.possible_values[row_i][col])

        # check for contraint count in grid neighbours
        grid_i_pos = row - (row)%self.M
        grid_j_pos = col - (col)%self.K         
        for row_i in range(grid_i_pos, grid_i_pos+self.M):
            for col_i in range(grid_j_pos, grid_j_pos+self.K):
                if row_i != row and col_i != col:
                    constraint_count += (val in self.possible_values[row_i][col_i])

        return constraint_count


    def get_dom_val_in_lcv_order(self, row, col):

        lcv_moves = sorted( self.possible_values[row][col], 
            key=lambda some_possible_val: self.get_count_of_constraints(row, col, some_possible_val) )
        return lcv_moves

    def all_cells_alive(self):
        
        # # Debug #
        # if len(self.dead_cells) != 0:
        #     print "dead_cells: ", self.dead_cells, "\n"
        #     print "current_status", self.sudoku_board, "\n"
        #     print "assigned: ", self.assigned, "\n"
        return (len(self.dead_cells) == 0)
    

    def is_row_conflict(self, row, col):
        """
        Check the number of conlicts (count of that element) in a row, 
        if any conflict return true 
        """
        
        value = self.sudoku_board[row][col]
        return (self.sudoku_board[row].count(value) > 1)

    def is_column_conflict(self, row, col):
        """
        Check the number of conlicts (count of that element) in a col, 
        if any conflict return true
        """

        value = self.sudoku_board[row][col]
        count = 0
        
        for row_i in range(0, self.N):
            if self.sudoku_board[row_i][col] == value:
                count += 1
                
                if count > 1: # return when first conflict found, with true value
                    return True
        
        return False
    
    def is_grid_conflict(self, row, col):
        """
        Check the number of conlicts (count of that element) in a grid, 
        if any conflict return true
        """

        value = self.sudoku_board[row][col]
        count = 0
        
        # find the pos of starting of corresponding grid
        pos_i = row - (row)%self.M
        pos_j = col - (col)%self.K 
        
        for row_i in range(pos_i, pos_i+self.M):
            for col_j in range(pos_j, pos_j+self.K):

                if self.sudoku_board[row_i][col_j] == value:
                    count+=1
                    
                    if count >1: # short-circuit
                        return True
        
        return False

    def get_min_conflict_value(self, row, col):
        """
        For variable reprented by (row,col), 
        find value which is least conflicting
        """

        conflict_count = [0 for x in range(0, self.N)]

        # Debug #
        # print "**** N= ", self.N, " conflict_count= ", conflict_count

        for col_i in range(0, self.N): # iterating on row
            conflict_count[self.sudoku_board[row][col_i]-1] += 1

        for row_i in range(0, self.N): # iterating on col
            conflict_count[self.sudoku_board[row_i][col]-1] += 1

        pos_i = row - (row)%self.M
        pos_j = col - (col)%self.K 

        for row_i in range(pos_i, pos_i+self.M):
            for col_j in range(pos_j, pos_j+self.K):
                conflict_count[self.sudoku_board[row_i][col_j]-1] += 1

        min_val = float('inf')

        for val in range(0, self.N):
            if conflict_count[val] < min_val:
                min_val = val+1

        return min_val
        


    def get_conflicted_cell(self):
        """
        returns randomly chosen conflicting cell, 
        if no conflicting cells, returns (-1, -1) 
        """
        
        conflicting_cells=[]
        
        for row in range(0,self.N):
            for col in range(0,self.N):

                if self.is_row_conflict(row, col) or self.is_column_conflict(row, col) \
                or self.is_grid_conflict(row, col):
                    conflicting_cells.append((row, col))

        if len(conflicting_cells) == 0:
            return (-1, -1)

        # Debug #
        # print "sudoku_baord=\n", self.sudoku_board, "\n"
        # print "conflicting_cells", conflicting_cells, "\n"

        return random.sample(conflicting_cells, 1)[0]

    def assign_blank_spaces(self):
        """
        Assign blank spaces, with values from 1-N
        """
        for row in range(0, self.N):
            for col in range(0, self.N):
                if ( self.sudoku_board[row][col] == -1 ):
                    self.assign_min_conflicting_value(row, col)
                    # self.sudoku_board[row][col] = random.sample( [x for x in range(1, self.N+1)], 1)[0]


    def assign_min_conflicting_value(self, row, col):
        """
        Assign minimum conflicting value to sudoku_board[row][col]
        """
        self.sudoku_board[row][col] = self.get_min_conflict_value(row, col)

    def testing_area(self):
        """
        For debugging purpose 
        """

        # Test_1
        # Test: is_assignment_complete function
        # print "is_assignment_complete\n", self.is_assignment_complete(), "\n"

        # Test_2
        # Test: generalte all possible moves and print# 
        # i = 0
        # for move in self.get_all_next_states():
        #     print move.sudoku_board, "\n\n"
        #     i += 1            
        #     if i == 20:
        #         break

        # Test_3
        # Test : test if is_consistent fine
        # i = 0
        # j = 0
        # print "consistency check for i, j, val: ", i, j, self.is_consistent(i,j), "\n"

        # i = 1
        # j = 3
        # print "consistency check for i, j, val: ", i, j, self.is_consistent(i,j), "\n"

        # i = 0
        # j = 5
        # print "consistency check for i, j, val: ", i, j, self.is_consistent(i,j), "\n"


